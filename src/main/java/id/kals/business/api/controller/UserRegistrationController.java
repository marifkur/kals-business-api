package id.kals.business.api.controller;

import id.kals.business.api.request.UserRegistrationRequest;
import id.kals.business.model.entity.*;
import id.kals.business.model.enums.Status;
import id.kals.business.model.exceptions.DataNotFoundException;
import id.kals.business.model.model.Error;
import id.kals.business.model.model.JsonResponse;
import id.kals.business.model.model.ResultResponse;
import id.kals.business.model.model.Source;
import id.kals.business.model.service.CategoryTypeService;
import id.kals.business.model.service.UsersService;
import id.kals.business.model.utils.MySHA512PasswordEncoder;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.WebRequest;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @Author arif <m.arif.kurniawan@myindo.co.id>
 * @Since 9/14/20
 */
@Slf4j
@RestController
@RequestMapping("/users")
public class UserRegistrationController {

    @Autowired
    private UsersService usersService;

    @Autowired
    private CategoryTypeService categoryTypeService;

    private String USERS_REGISTRATION_RESPONSE = "userRegistration";

    private String USER_REGISTRATION_DELETE_RESPONSE = "userRegistrationDelete";

    @PostMapping(value = "registration/add")
    public ResponseEntity<?> userAdminRegistrationEmailAddPost(@Valid @RequestBody UserRegistrationRequest userRegistrationRequest,
                                                               Errors errors, WebRequest request, HttpServletRequest httpServletRequest) {


        JsonResponse<Map<String, Object>> response = new JsonResponse();
        Map<String, Object> data = new HashMap();
        Map<String, Object> object = new HashMap();

        Error error = new Error();
        error.setCode("400");
        error.setStatus(HttpStatus.BAD_REQUEST.toString());

        Users userPre = new Users();

        Personal personalPre = new Personal();

        if (errors.hasErrors()) {

            errors.getAllErrors().forEach(ex -> {
                Source source = new Source();
                source.setPointer(request.getDescription(false));
                source.setParameter(new String());

                error.setSource(source);
                error.setDetail(ex.getDefaultMessage());
                error.setTitle("VALIDATION FAILED");
                error.setStatus(HttpStatus.BAD_REQUEST.getReasonPhrase());
                response.getErrors().add(error);
            });

            data.put(USERS_REGISTRATION_RESPONSE, userRegistrationRequest);
            response.setData(data);

            return new ResponseEntity(response, HttpStatus.BAD_REQUEST);
        }

        Pattern ptnEmail = Pattern.compile("^[\\w-_\\.+]*[\\w-_\\.]\\@([\\w]+\\.)+[\\w]+[\\w]$");
        Matcher matcherEmail = ptnEmail.matcher(userRegistrationRequest.getUsername().toLowerCase());

        if (!matcherEmail.matches()) {

            error.setTitle("Username Not Valid");
            error.setDetail("Username Not Valid (Please Input your username with valid email)");

            response.getErrors().add(error);
            data.put(USERS_REGISTRATION_RESPONSE, userRegistrationRequest);
            response.setData(data);

            return new ResponseEntity(response, HttpStatus.BAD_REQUEST);
        }

        if (!userRegistrationRequest.getPassword().equals(userRegistrationRequest.getRetypePassword())) {

            error.setTitle("Password Not Valid");
            error.setDetail("Password Not Same with ReTypePassword");

            response.getErrors().add(error);
            data.put(USERS_REGISTRATION_RESPONSE, userRegistrationRequest);
            response.setData(data);

            return new ResponseEntity(response, HttpStatus.BAD_REQUEST);
        }

        Pattern ptnPassword = Pattern.compile("^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)[a-zA-Z\\d]{6,}$");
        Matcher matcherPassword = ptnPassword.matcher(userRegistrationRequest.getPassword());

        if (!matcherPassword.matches()) {
            error.setTitle("Password Not Valid");
            error.setDetail("Password min 8 character include lowercase, uppercase and numeric character");

            data.put(USERS_REGISTRATION_RESPONSE, userRegistrationRequest);
            response.setData(data);
            response.getErrors().add(error);

            return new ResponseEntity(response, HttpStatus.BAD_REQUEST);
        }

        Users users = usersService.findByUsernameAndStatus(userRegistrationRequest.getUsername(), Status.ACTIVE);

        if (users != null) {

            error.setCode("202");
            error.setStatus(HttpStatus.ACCEPTED.toString());
            error.setTitle("Username Not Valid");
            error.setDetail("Username Already Exist");
            data.put(USERS_REGISTRATION_RESPONSE, userRegistrationRequest);
            response.setData(data);
            response.getErrors().add(error);

            return new ResponseEntity(response, HttpStatus.ACCEPTED);
        }

        CategoryType categoryType = categoryTypeService.findByIdAndStatus(userRegistrationRequest.getCategoryTypeId(), Status.ACTIVE);

        if (categoryType == null) {

            error.setCode("202");
            error.setStatus(HttpStatus.ACCEPTED.toString());
            error.setTitle("categoryType Not Found");
            error.setDetail("categoryType Not Found");
            data.put(USERS_REGISTRATION_RESPONSE, userRegistrationRequest);
            response.setData(data);
            response.getErrors().add(error);
            return new ResponseEntity(response, HttpStatus.ACCEPTED);
        }


        try {
            // Password
            PasswordEncoder mySHA512PasswordEncoder = new MySHA512PasswordEncoder();
            String password = mySHA512PasswordEncoder.encode(userRegistrationRequest.getPassword());
            String[] hash = password.split("\\$");
            String salt = "$" + hash[1] + "$" + hash[2] + "$";


            //Add New User
            userPre.setPassword(password);
            userPre.setSalt(salt);
            userPre.setUsername(userRegistrationRequest.getUsername());
            userPre.setStatus(Status.DISABLE);

            userPre.setCreateBy("system");
            userPre.setModifiedBy("system");
            userPre.setCreateAt(LocalDateTime.now());
            userPre.setCreateIp(httpServletRequest.getRemoteAddr());
            userPre.setModifiedIp(httpServletRequest.getRemoteAddr());


            personalPre.setNickName(userRegistrationRequest.getNickName());
            personalPre.setFirstName(userRegistrationRequest.getFirstName());
            personalPre.setLastName(userRegistrationRequest.getLastName());
            personalPre.setMaritalStatus(userRegistrationRequest.getMaritalStatus());
            personalPre.setPlaceOfBirth(userRegistrationRequest.getPlaceOfBirth());
            personalPre.setMotherName(userRegistrationRequest.getMotherName());
            personalPre.setBirthDate(userRegistrationRequest.getBirthDate());
            personalPre.setGender(userRegistrationRequest.getGender());
            personalPre.setReligion(userRegistrationRequest.getReligion());

            personalPre.setStatus(Status.ACTIVE);
            personalPre.setCreateBy("system");
            personalPre.setModifiedBy("system");
            personalPre.setCreateAt(LocalDateTime.now());
            personalPre.setCreateIp(httpServletRequest.getRemoteAddr());
            personalPre.setModifiedIp(httpServletRequest.getRemoteAddr());

            ResultResponse resultResponse = usersService.registerUser(userPre, personalPre, categoryType);


            data.put("email", userRegistrationRequest.getUsername());
            object.put(USERS_REGISTRATION_RESPONSE, data);
            response.setData(object);

        } catch (Exception e) {
            e.printStackTrace();

            error.setCode("500");
            error.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.toString());
            error.setTitle(e.getMessage());
            error.setDetail(e.getMessage());
            response.getErrors().add(error);
            return new ResponseEntity(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }


        return new ResponseEntity(response, HttpStatus.CREATED);
    }

    @DeleteMapping("/registration/delete")
    public ResponseEntity userRegisteredDelete(@RequestParam("username") String username) {
        JsonResponse<Map<String, Object>> response = new JsonResponse<>();
        Map<String, Object> data = new HashMap<>();

        Map<String, Object> params = new LinkedHashMap<>();
        params.put("username", username);

        Map<String, Object> dictionaries = new LinkedHashMap<>();
        dictionaries.put("params", params);
        response.setDictionaries(dictionaries);


        try {
            ResultResponse resultResponse = usersService.deleteUserRegistered(username);
            data.put(USER_REGISTRATION_DELETE_RESPONSE, resultResponse);
            response.setData(data);
            return new ResponseEntity(response, HttpStatus.OK);

        } catch (DataNotFoundException dnfe) {
            Error error = new Error();
            error.setCode("202");
            error.setStatus(HttpStatus.ACCEPTED.getReasonPhrase());
            error.setTitle(dnfe.getLocalizedMessage());
            error.setDetail(dnfe.getLocalizedMessage());
            response.getErrors().add(error);
            return new ResponseEntity(response, HttpStatus.ACCEPTED);

        } catch (Exception e) {
            Error error = new Error();
            error.setCode("500");
            error.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.toString());
            error.setTitle(e.getMessage());
            error.setDetail(e.getMessage());
            response.getErrors().add(error);
            response.setData(data);
            return new ResponseEntity(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
