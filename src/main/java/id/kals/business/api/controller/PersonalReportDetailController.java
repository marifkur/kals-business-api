package id.kals.business.api.controller;

import id.kals.business.api.request.PersonalReportDetailRequest;
import id.kals.business.model.entity.Personal;
import id.kals.business.model.entity.PersonalReport;
import id.kals.business.model.entity.PersonalReportDetail;
import id.kals.business.model.enums.Status;
import id.kals.business.model.model.Error;
import id.kals.business.model.model.JsonResponse;
import id.kals.business.model.model.Source;
import id.kals.business.model.service.PersonalReportDetailService;
import id.kals.business.model.service.PersonalReportService;
import id.kals.business.model.service.PersonalService;
import id.kals.business.model.utils.BaseSpecification;
import id.kals.business.model.utils.PageUtils;
import id.kals.business.model.utils.SearchCriteria;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.WebRequest;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Author arif <m.arif.kurniawan@myindo.co.id>
 * @Since 9/14/20
 */
@Slf4j
@RestController
@RequestMapping("/personalReportDetail")
public class PersonalReportDetailController {

    @Autowired
    private PersonalReportDetailService personalReportDetailService;

    @Autowired
    private PersonalReportService personalReportService;

    @Autowired
    private PersonalService personalService;

    private String PERSONAL_REPORT_DETAIL_RESPONSE = "personalReportDetailDetail";

    private String PERSONAL_REPORT_DETAIL_DELETE_RESPONSE = "personalReportDetailDelete";

    @GetMapping(value = "list")
    public ResponseEntity<?> personalReportDetailListGet(@RequestParam("orderBy") String orderBy,
                                                   @RequestParam("sortBy") String sortBy,
                                                   @RequestParam("page") int page,
                                                   @RequestParam("size") int size,
                                                   @RequestParam(required = false, defaultValue = "") String personalReportId,
                                                   @RequestParam(required = false, defaultValue = "") String type) {


        BaseSpecification<PersonalReportDetail> spec2 =
                new BaseSpecification<PersonalReportDetail>(new SearchCriteria("personalReport.id", ":", personalReportId));

        BaseSpecification<PersonalReportDetail> spec3 =
                new BaseSpecification<PersonalReportDetail>(new SearchCriteria("type", ":", type));

        BaseSpecification<PersonalReportDetail> spec11 =
                new BaseSpecification<PersonalReportDetail>(new SearchCriteria("status", ":", Status.DELETE));


        Specification<PersonalReportDetail> specGroup = Specification.where(spec2).and(spec3).and(Specification.not(spec11));
        JsonResponse<List<PersonalReportDetail>> response = new JsonResponse<>();
        Map<String, Object> dictionaries = new HashMap<>();
        Map<String, Object> data = new HashMap<>();

        PageUtils pageUtils = new PageUtils();
        try {

            Page<PersonalReportDetail> list = personalReportDetailService.findAllSpecification(specGroup, pageUtils.pagingAndSorting(orderBy, sortBy, page - 1, size));
            if (list == null) {
                Error error = new Error();
                error.setCode("202");
                error.setStatus(HttpStatus.ACCEPTED.toString());
                error.setTitle("Data Not Found");
                error.setDetail("List Not Found Or Empty");
            }

            List<PersonalReportDetail> personalReportDetailList = list.getContent();

            dictionaries.put("totalPages", list.getTotalPages());
            dictionaries.put("totalElements", list.getTotalElements());
            dictionaries.put("numberOfElements", list.getNumberOfElements());
            dictionaries.put("pageSize", list.getSize());
            dictionaries.put("number", list.getNumber());
            dictionaries.put("orderBy", orderBy);
            dictionaries.put("sortBy", sortBy);
            data.put("pagging", dictionaries);

            response.setData(personalReportDetailList);
            response.setDictionaries(data);

        } catch (Exception ex) {

            Error error = new Error();
            error.setCode("500");
            error.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.toString());
            error.setTitle("Something Error");
            error.setDetail("Something Error, please contact developer");

            return new ResponseEntity(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return new ResponseEntity<>(response, HttpStatus.OK);

    }


    @PostMapping(value = "add")
    public ResponseEntity<?> personalReportDetailAddPost(
            WebRequest request, HttpServletRequest httpServletRequest,
            @Valid @RequestBody PersonalReportDetailRequest personalReportDetailRequest, Errors errors) {

        JsonResponse<Map<String, Object>> response = new JsonResponse();
        Map<String, Object> data = new HashMap();
        PersonalReportDetail personalReportDetail = new PersonalReportDetail();

        if (errors.hasErrors()) {

            errors.getAllErrors().forEach(ex -> {
                Source source = new Source();
                source.setPointer(request.getDescription(false));
                source.setParameter(new String());

                Error error = new Error();
                error.setCode("400");
                error.setSource(source);
                error.setDetail(ex.getDefaultMessage());
                error.setTitle("Validation Failed");
                error.setStatus(HttpStatus.BAD_REQUEST.getReasonPhrase());

                response.getErrors().add(error);

            });
            data.put(PERSONAL_REPORT_DETAIL_RESPONSE, personalReportDetailRequest);
            response.setData(data);
            return new ResponseEntity(response, HttpStatus.BAD_REQUEST);
        }

        try {
            PersonalReport personalReport = personalReportService.findByIdAndStatus(personalReportDetailRequest.getPersonalReportId(), Status.ACTIVE);

            if (personalReport == null) {

                Error error = new Error();
                error.setCode("202");
                error.setStatus(HttpStatus.ACCEPTED.toString());
                error.setTitle("personalReport Not Found");
                error.setDetail("personalReport Not Found");
                response.getErrors().add(error);
                return new ResponseEntity(response, HttpStatus.ACCEPTED);
            }
            BeanUtils.copyProperties(personalReportDetailRequest, personalReportDetail);

            personalReportDetail.setCreateAt(LocalDateTime.now());
            personalReportDetail.setCreateBy("system");
            personalReportDetail.setCreateIp(httpServletRequest.getRemoteAddr());
            personalReportDetail.setCost(personalReportDetailRequest.getCost());
            personalReportDetail.setStatus(Status.ACTIVE);
            if (personalReportDetailRequest.getStatus() != null)
                personalReportDetail.setStatus(personalReportDetailRequest.getStatus());


            PersonalReportDetail personalReportDetailSave = personalReportDetailService.save(personalReportDetail);
            data.put(PERSONAL_REPORT_DETAIL_RESPONSE, personalReportDetailSave);
            response.setData(data);
        } catch (Exception ex) {
            Error error = new Error();
            error.setCode("500");
            error.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.toString());
            error.setTitle(ex.getMessage());
            error.setDetail(ex.getMessage());
            return new ResponseEntity(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity(response, HttpStatus.CREATED);
    }

    @PutMapping(value = "update")
    public ResponseEntity<?> personalReportDetailUpdateIdPut(WebRequest request, HttpServletRequest httpServletRequest,
                                                       @Valid @RequestBody PersonalReportDetailRequest personalReportDetailRequest, Errors errors, @RequestParam("id") String id) {

        JsonResponse<Map<String, Object>> response = new JsonResponse();

        Map<String, Object> data = new HashMap();
        Map<String, Object> params = new HashMap();
        params.put("id", id);

        Map<String, Object> dictionaries = new HashMap();
        dictionaries.put("params", params);
        response.setDictionaries(dictionaries);

        if (errors.hasErrors()) {

            errors.getAllErrors().forEach(ex -> {
                Source source = new Source();
                source.setPointer(request.getDescription(false));
                source.setParameter(new String());

                Error error = new Error();
                error.setCode("400");
                error.setSource(source);
                error.setDetail(ex.getDefaultMessage());
                error.setTitle("Validation Failed");
                error.setStatus(HttpStatus.BAD_REQUEST.getReasonPhrase());

                response.getErrors().add(error);

            });
            data.put(PERSONAL_REPORT_DETAIL_RESPONSE, personalReportDetailRequest);
            response.setData(data);
            return new ResponseEntity(response, HttpStatus.BAD_REQUEST);
        }

        try {
            PersonalReportDetail personalReportDetailValid = personalReportDetailService.findById(id);
            if (personalReportDetailValid == null) {
                Error error = new Error();
                error.setCode("202");
                error.setStatus(HttpStatus.ACCEPTED.toString());
                error.setTitle("Personal Report Detail Not Found");
                error.setDetail("Personal Report Detail Not found");

                return new ResponseEntity(response, HttpStatus.ACCEPTED);
            }

            PersonalReport personalReport = personalReportService.findByIdAndStatus(personalReportDetailRequest.getPersonalReportId(), Status.ACTIVE);

            if (personalReport == null) {

                Error error = new Error();
                error.setCode("202");
                error.setStatus(HttpStatus.ACCEPTED.toString());
                error.setTitle("personalReport Not Found");
                error.setDetail("personalReport Not Found");
                response.getErrors().add(error);
                return new ResponseEntity(response, HttpStatus.ACCEPTED);
            }
            BeanUtils.copyProperties(personalReportDetailRequest, personalReportDetailValid);

            personalReportDetailValid.setModifiedAt(LocalDateTime.now());
            personalReportDetailValid.setModifiedBy("system");
            personalReportDetailValid.setModifiedIp(httpServletRequest.getRemoteAddr());
            personalReportDetailValid.setCost(personalReportDetailRequest.getCost());
            personalReportDetailValid.setStatus(Status.ACTIVE);
            if (personalReportDetailRequest.getStatus() != null)
                personalReportDetailValid.setStatus(personalReportDetailRequest.getStatus());


            PersonalReportDetail personalReportDetailSave = personalReportDetailService.save(personalReportDetailValid);
            data.put(PERSONAL_REPORT_DETAIL_RESPONSE, personalReportDetailSave);
            response.setData(data);

        } catch (Exception ex) {
            Error error = new Error();
            error.setCode("500");
            error.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.toString());
            error.setTitle(ex.getMessage());
            error.setDetail(ex.getMessage());

            return new ResponseEntity(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity(response, HttpStatus.OK);
    }

    @DeleteMapping(value = "delete")
    public ResponseEntity<Map<String, Object>> personalReportDetailDeleteIdDelete(WebRequest request,
                                                                            HttpServletRequest httpServletRequest,
                                                                            @RequestParam("id") String id) {

        JsonResponse<Map<String, Object>> response = new JsonResponse();

        Map<String, Object> data = new HashMap();
        Map<String, Object> params = new HashMap();
        params.put("id", id);

        Map<String, Object> dictionaries = new HashMap();
        dictionaries.put("params", params);
        response.setDictionaries(dictionaries);
        try {
            PersonalReportDetail personalReportDetail = personalReportDetailService.findById(id);
            if (personalReportDetail == null) {
                Error error = new Error();
                error.setCode("202");
                error.setStatus(HttpStatus.ACCEPTED.toString());
                error.setTitle("Personal Report Detail Not Found");
                error.setDetail("Personal Report Detail Not found");

                return new ResponseEntity(response, HttpStatus.ACCEPTED);
            }

            personalReportDetail.setModifiedIp(httpServletRequest.getRemoteAddr());
            personalReportDetail.setModifiedAt(LocalDateTime.now());
            personalReportDetail.setModifiedBy("system");
            personalReportDetail.setStatus(Status.DELETE);

            personalReportDetailService.delete(personalReportDetail);

            data.put(PERSONAL_REPORT_DETAIL_DELETE_RESPONSE, personalReportDetail);

            response.setData(data);

        } catch (Exception ex) {
            Error error = new Error();
            error.setCode("500");
            error.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.toString());
            error.setTitle(ex.getMessage());
            error.setDetail(ex.getMessage());

            return new ResponseEntity(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return new ResponseEntity(response, HttpStatus.OK);

    }

    @GetMapping(value = "detail")
    public ResponseEntity<JsonResponse<PersonalReportDetail>> personalReportDetailDetailIdGet(WebRequest request, @RequestParam("id") String id) {

        JsonResponse<Map<String, Object>> response = new JsonResponse();

        Map<String, Object> data = new HashMap();
        Map<String, Object> params = new HashMap();
        params.put("id", id);

        Map<String, Object> dictionaries = new HashMap();
        dictionaries.put("params", params);
        response.setDictionaries(dictionaries);

        try {
            PersonalReportDetail personalReportDetail = personalReportDetailService.findById(id);
            if (personalReportDetail == null) {
                Error error = new Error();
                error.setCode("202");
                error.setStatus(HttpStatus.ACCEPTED.toString());
                error.setTitle("Personal Report Detail Not Found");
                error.setDetail("Personal Report Not Detail found");

                return new ResponseEntity(response, HttpStatus.ACCEPTED);
            }
            data.put(PERSONAL_REPORT_DETAIL_RESPONSE, personalReportDetail);
            response.setData(data);

        } catch (Exception ex) {
            Error error = new Error();
            error.setCode("500");
            error.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.toString());
            error.setTitle(ex.getMessage());
            error.setDetail(ex.getMessage());

            return new ResponseEntity(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity(response, HttpStatus.OK);
    }

}
