package id.kals.business.api.controller;

import id.kals.business.api.request.PaymentRequest;
import id.kals.business.model.entity.*;
import id.kals.business.model.enums.Status;
import id.kals.business.model.model.Error;
import id.kals.business.model.model.JsonResponse;
import id.kals.business.model.model.Source;
import id.kals.business.model.service.PaymentMethodService;
import id.kals.business.model.service.PaymentService;
import id.kals.business.model.service.PersonalReportService;
import id.kals.business.model.service.PersonalService;
import id.kals.business.model.utils.BaseSpecification;
import id.kals.business.model.utils.PageUtils;
import id.kals.business.model.utils.SearchCriteria;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.WebRequest;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Author arif <m.arif.kurniawan@myindo.co.id>
 * @Since 9/14/20
 */
@Slf4j
@RestController
@RequestMapping("/payment")
public class PaymentController {

    @Autowired
    private PaymentService paymentService;

    @Autowired
    private PersonalService personalService;

    @Autowired
    private PersonalReportService personalReportService;

    @Autowired
    private PaymentMethodService paymentMethodService;

    private String PAYMENT_RESPONSE = "paymentDetail";
    private String PAYMENT_DELETE_RESPONSE = "paymentDelete";

    @GetMapping(value = "list")
    public ResponseEntity<?> paymentListGet(@RequestParam("orderBy") String orderBy,
                                            @RequestParam("sortBy") String sortBy,
                                            @RequestParam("page") int page,
                                            @RequestParam("size") int size,
                                            @RequestParam(required = false, defaultValue = "") String personalReportId,
                                            @RequestParam(required = false, defaultValue = "") String month,
                                            @RequestParam(required = false, defaultValue = "") String monthYear,
                                            @RequestParam(required = false, defaultValue = "") String year) {


        BaseSpecification<Payment> spec2 =
                new BaseSpecification<Payment>(new SearchCriteria("personalReport.id", ":", personalReportId));

        BaseSpecification<Payment> spec3 =
                new BaseSpecification<Payment>(new SearchCriteria("paymentDate~month", ":", month));

        BaseSpecification<Payment> spec5 =
                new BaseSpecification<Payment>(new SearchCriteria("paymentDate~month-year", ":", monthYear));

        BaseSpecification<Payment> spec4 =
                new BaseSpecification<Payment>(new SearchCriteria("paymentDate~year", ":", year));


        BaseSpecification<Payment> spec11 =
                new BaseSpecification<Payment>(new SearchCriteria("status", ":", Status.DELETE));


        Specification<Payment> specGroup = Specification.where(spec2).and(spec3).and(spec4).and(Specification.not(spec11));
        JsonResponse<List<Payment>> response = new JsonResponse<>();
        Map<String, Object> dictionaries = new HashMap<>();
        Map<String, Object> data = new HashMap<>();

        PageUtils pageUtils = new PageUtils();
        try {

            Page<Payment> list = paymentService.findAllSpecification(specGroup, pageUtils.pagingAndSorting(orderBy, sortBy, page - 1, size));
            if (list == null) {
                Error error = new Error();
                error.setCode("202");
                error.setStatus(HttpStatus.ACCEPTED.toString());
                error.setTitle("Data Not Found");
                error.setDetail("List Not Found Or Empty");
            }

            List<Payment> paymentList = list.getContent();

            dictionaries.put("totalPages", list.getTotalPages());
            dictionaries.put("totalElements", list.getTotalElements());
            dictionaries.put("numberOfElements", list.getNumberOfElements());
            dictionaries.put("pageSize", list.getSize());
            dictionaries.put("number", list.getNumber());
            dictionaries.put("orderBy", orderBy);
            dictionaries.put("sortBy", sortBy);
            data.put("pagging", dictionaries);

            response.setData(paymentList);
            response.setDictionaries(data);

        } catch (Exception ex) {

            Error error = new Error();
            error.setCode("500");
            error.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.toString());
            error.setTitle("Something Error");
            error.setDetail("Something Error, please contact developer");

            return new ResponseEntity(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return new ResponseEntity<>(response, HttpStatus.OK);

    }


    @PostMapping(value = "add")
    public ResponseEntity<?> paymentAddPost(
            WebRequest request, HttpServletRequest httpServletRequest,
            @Valid @RequestBody PaymentRequest paymentRequest, Errors errors) {

        JsonResponse<Map<String, Object>> response = new JsonResponse();
        Map<String, Object> data = new HashMap();
        Payment payment = new Payment();

        if (errors.hasErrors()) {

            errors.getAllErrors().forEach(ex -> {
                Source source = new Source();
                source.setPointer(request.getDescription(false));
                source.setParameter(new String());

                Error error = new Error();
                error.setCode("400");
                error.setSource(source);
                error.setDetail(ex.getDefaultMessage());
                error.setTitle("Validation Failed");
                error.setStatus(HttpStatus.BAD_REQUEST.getReasonPhrase());

                response.getErrors().add(error);

            });
            data.put(PAYMENT_RESPONSE, paymentRequest);
            response.setData(data);
            return new ResponseEntity(response, HttpStatus.BAD_REQUEST);
        }

        PersonalReport personalReport = personalReportService.findByIdAndStatus(paymentRequest.getPersonalReportId(), Status.ACTIVE);

        if (personalReport == null) {

            Error error = new Error();
            error.setCode("202");
            error.setStatus(HttpStatus.ACCEPTED.toString());
            error.setTitle("personalReport Not Found");
            error.setDetail("personalReport Not Found");
            response.getErrors().add(error);
            return new ResponseEntity(response, HttpStatus.ACCEPTED);
        }

        PaymentMethod paymentMethod = paymentMethodService.findByIdAndStatus(paymentRequest.getPaymentMethodId(), Status.ACTIVE);

        if (paymentMethod == null) {

            Error error = new Error();
            error.setCode("202");
            error.setStatus(HttpStatus.ACCEPTED.toString());
            error.setTitle("paymentMethod Not Found");
            error.setDetail("paymentMethod Not Found");
            response.getErrors().add(error);
            return new ResponseEntity(response, HttpStatus.ACCEPTED);
        }
        List<PersonalReportDetail> personalReportDetailList = personalReport.getPersonalReportDetailList().isEmpty() ? null : personalReport.getPersonalReportDetailList();
        Double total = 0.0;
        if (personalReportDetailList != null) {
            for (PersonalReportDetail personalReportDetail : personalReportDetailList) {
                total += personalReportDetail.getCost();
            }
        }
        try {

            BeanUtils.copyProperties(paymentRequest, payment);
            payment.setPersonalReport(personalReport);
            payment.setPaymentMethod(paymentMethod);
            payment.setPaymentDate(LocalDateTime.now());
            payment.setStatus(Status.ACTIVE);
            if (paymentRequest.getStatus() != null)
                payment.setStatus(paymentRequest.getStatus());
            payment.setTotal(total);
            payment.setCreateAt(LocalDateTime.now());
            payment.setCreateBy("system");
            payment.setCreateIp(httpServletRequest.getRemoteAddr());

            Payment paymentSave = paymentService.save(payment);
            data.put(PAYMENT_RESPONSE, paymentSave);
            response.setData(data);
        } catch (Exception ex) {
            Error error = new Error();
            error.setCode("500");
            error.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.toString());
            error.setTitle(ex.getMessage());
            error.setDetail(ex.getMessage());
            return new ResponseEntity(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity(response, HttpStatus.CREATED);
    }

    @PutMapping(value = "update")
    public ResponseEntity<?> paymentUpdateIdPut(WebRequest request, HttpServletRequest httpServletRequest,
                                                @Valid @RequestBody PaymentRequest paymentRequest, Errors errors, @RequestParam("id") String id) {

        JsonResponse<Map<String, Object>> response = new JsonResponse();

        Map<String, Object> data = new HashMap();
        Map<String, Object> params = new HashMap();
        params.put("id", id);

        Map<String, Object> dictionaries = new HashMap();
        dictionaries.put("params", params);
        response.setDictionaries(dictionaries);

        if (errors.hasErrors()) {

            errors.getAllErrors().forEach(ex -> {
                Source source = new Source();
                source.setPointer(request.getDescription(false));
                source.setParameter(new String());

                Error error = new Error();
                error.setCode("400");
                error.setSource(source);
                error.setDetail(ex.getDefaultMessage());
                error.setTitle("Validation Failed");
                error.setStatus(HttpStatus.BAD_REQUEST.getReasonPhrase());

                response.getErrors().add(error);

            });
            data.put(PAYMENT_RESPONSE, paymentRequest);
            response.setData(data);
            return new ResponseEntity(response, HttpStatus.BAD_REQUEST);
        }

        try {
            Payment paymentValid = paymentService.findById(id);
            if (paymentValid == null) {
                Error error = new Error();
                error.setCode("202");
                error.setStatus(HttpStatus.ACCEPTED.toString());
                error.setTitle("Payment Not Found");
                error.setDetail("Payment Not found");

                return new ResponseEntity(response, HttpStatus.ACCEPTED);
            }

            PersonalReport personalReport = personalReportService.findByIdAndStatus(paymentRequest.getPersonalReportId(), Status.ACTIVE);

            if (personalReport == null) {

                Error error = new Error();
                error.setCode("202");
                error.setStatus(HttpStatus.ACCEPTED.toString());
                error.setTitle("personalReport Not Found");
                error.setDetail("personalReport Not Found");
                response.getErrors().add(error);
                return new ResponseEntity(response, HttpStatus.ACCEPTED);
            }

            PaymentMethod paymentMethod = paymentMethodService.findByIdAndStatus(paymentRequest.getPaymentMethodId(), Status.ACTIVE);

            if (paymentMethod == null) {

                Error error = new Error();
                error.setCode("202");
                error.setStatus(HttpStatus.ACCEPTED.toString());
                error.setTitle("paymentMethod Not Found");
                error.setDetail("paymentMethod Not Found");
                response.getErrors().add(error);
                return new ResponseEntity(response, HttpStatus.ACCEPTED);
            }
            List<PersonalReportDetail> personalReportDetailList = personalReport.getPersonalReportDetailList().isEmpty() ? null : personalReport.getPersonalReportDetailList();
            Double total = 0.0;
            if (personalReportDetailList != null) {
                for (PersonalReportDetail personalReportDetail : personalReportDetailList) {
                    total += personalReportDetail.getCost();
                }
            }

            BeanUtils.copyProperties(paymentRequest, paymentValid);
            paymentValid.setPersonalReport(personalReport);
            paymentValid.setPaymentMethod(paymentMethod);
            if (paymentRequest.getPaymentDate() != null)
                paymentValid.setPaymentDate(paymentRequest.getPaymentDate());
            paymentValid.setStatus(Status.ACTIVE);
            if (paymentRequest.getStatus() != null)
                paymentValid.setStatus(paymentRequest.getStatus());
            paymentValid.setTotal(total);
            paymentValid.setCreateAt(LocalDateTime.now());
            paymentValid.setCreateBy("system");
            paymentValid.setCreateIp(httpServletRequest.getRemoteAddr());

            Payment paymentSave = paymentService.save(paymentValid);
            data.put(PAYMENT_RESPONSE, paymentSave);
            response.setData(data);

        } catch (Exception ex) {
            Error error = new Error();
            error.setCode("500");
            error.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.toString());
            error.setTitle(ex.getMessage());
            error.setDetail(ex.getMessage());

            return new ResponseEntity(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity(response, HttpStatus.OK);
    }

    @DeleteMapping(value = "delete")
    public ResponseEntity<Map<String, Object>> paymentDeleteIdDelete(WebRequest request,
                                                                     HttpServletRequest httpServletRequest,
                                                                     @RequestParam("id") String id) {

        JsonResponse<Map<String, Object>> response = new JsonResponse();

        Map<String, Object> data = new HashMap();
        Map<String, Object> params = new HashMap();
        params.put("id", id);

        Map<String, Object> dictionaries = new HashMap();
        dictionaries.put("params", params);
        response.setDictionaries(dictionaries);
        try {
            Payment payment = paymentService.findById(id);
            if (payment == null) {
                Error error = new Error();
                error.setCode("202");
                error.setStatus(HttpStatus.ACCEPTED.toString());
                error.setTitle("Payment Not Found");
                error.setDetail("Payment Not found");

                return new ResponseEntity(response, HttpStatus.ACCEPTED);
            }

            payment.setModifiedIp(httpServletRequest.getRemoteAddr());
            payment.setModifiedAt(LocalDateTime.now());
            payment.setModifiedBy("system");
            payment.setStatus(Status.DELETE);

            paymentService.delete(payment);

            data.put(PAYMENT_DELETE_RESPONSE, payment);

            response.setData(data);

        } catch (Exception ex) {
            Error error = new Error();
            error.setCode("500");
            error.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.toString());
            error.setTitle(ex.getMessage());
            error.setDetail(ex.getMessage());

            return new ResponseEntity(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return new ResponseEntity(response, HttpStatus.OK);

    }

    @GetMapping(value = "detail")
    public ResponseEntity<JsonResponse<Payment>> paymentDetailIdGet(WebRequest request, @RequestParam("id") String id) {

        JsonResponse<Map<String, Object>> response = new JsonResponse();

        Map<String, Object> data = new HashMap();
        Map<String, Object> params = new HashMap();
        params.put("id", id);

        Map<String, Object> dictionaries = new HashMap();
        dictionaries.put("params", params);
        response.setDictionaries(dictionaries);

        try {
            Payment payment = paymentService.findById(id);
            if (payment == null) {
                Error error = new Error();
                error.setCode("202");
                error.setStatus(HttpStatus.ACCEPTED.toString());
                error.setTitle("Payment Not Found");
                error.setDetail("Payment Not found");

                return new ResponseEntity(response, HttpStatus.ACCEPTED);
            }
            data.put(PAYMENT_RESPONSE, payment);
            response.setData(data);

        } catch (Exception ex) {
            Error error = new Error();
            error.setCode("500");
            error.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.toString());
            error.setTitle(ex.getMessage());
            error.setDetail(ex.getMessage());


            return new ResponseEntity(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity(response, HttpStatus.OK);
    }

}
