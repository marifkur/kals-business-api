package id.kals.business.api.controller;

import id.kals.business.api.request.PaymentMethodRequest;
import id.kals.business.model.entity.PaymentMethod;
import id.kals.business.model.enums.Status;
import id.kals.business.model.model.Error;
import id.kals.business.model.model.JsonResponse;
import id.kals.business.model.model.Source;
import id.kals.business.model.service.PaymentMethodService;
import id.kals.business.model.service.PersonalService;
import id.kals.business.model.utils.BaseSpecification;
import id.kals.business.model.utils.PageUtils;
import id.kals.business.model.utils.SearchCriteria;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.WebRequest;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Author arif <m.arif.kurniawan@myindo.co.id>
 * @Since 9/14/20
 */
@Slf4j
@RestController
@RequestMapping("/paymentMethod")
public class PaymentMethodController {

    @Autowired
    private PaymentMethodService paymentMethodService;

    @Autowired
    private PersonalService personalService;

    private String PERSONAL_REPORT_RESPONSE = "paymentMethodDetail";
    private String PERSONAL_REPORT_DELETE_RESPONSE = "paymentMethodDelete";

    @GetMapping(value = "list")
    public ResponseEntity<?> paymentMethodListGet(@RequestParam("orderBy") String orderBy,
                                         @RequestParam("sortBy") String sortBy,
                                         @RequestParam("page") int page,
                                         @RequestParam("size") int size,
                                         @RequestParam(required = false, defaultValue = "") String name) {


        BaseSpecification<PaymentMethod> spec1 =
                new BaseSpecification<PaymentMethod>(new SearchCriteria("name", ":", name));

        BaseSpecification<PaymentMethod> spec2 =
                new BaseSpecification<PaymentMethod>(new SearchCriteria("status", ":", Status.DELETE));


        Specification<PaymentMethod> specGroup = Specification.where(spec1).and(Specification.not(spec2));
        JsonResponse<List<PaymentMethod>> response = new JsonResponse<>();
        Map<String, Object> dictionaries = new HashMap<>();
        Map<String, Object> data = new HashMap<>();

        PageUtils pageUtils = new PageUtils();
        try {

            Page<PaymentMethod> list = paymentMethodService.findAllSpecification(specGroup, pageUtils.pagingAndSorting(orderBy, sortBy, page - 1, size));
            if (list == null) {
                Error error = new Error();
                error.setCode("202");
                error.setStatus(HttpStatus.ACCEPTED.toString());
                error.setTitle("Data Not Found");
                error.setDetail("List Not Found Or Empty");
            }

            List<PaymentMethod> paymentMethodList = list.getContent();

            dictionaries.put("totalPages", list.getTotalPages());
            dictionaries.put("totalElements", list.getTotalElements());
            dictionaries.put("numberOfElements", list.getNumberOfElements());
            dictionaries.put("pageSize", list.getSize());
            dictionaries.put("number", list.getNumber());
            dictionaries.put("orderBy", orderBy);
            dictionaries.put("sortBy", sortBy);
            data.put("pagging", dictionaries);

            response.setData(paymentMethodList);
            response.setDictionaries(data);

        } catch (Exception ex) {

            Error error = new Error();
            error.setCode("500");
            error.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.toString());
            error.setTitle("Something Error");
            error.setDetail("Something Error, please contact developer");

            return new ResponseEntity(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return new ResponseEntity<>(response, HttpStatus.OK);

    }


    @PostMapping(value = "add")
    public ResponseEntity<?> paymentMethodAddPost(
            WebRequest request, HttpServletRequest httpServletRequest,
            @Valid @RequestBody PaymentMethodRequest paymentMethodRequest, Errors errors) {

        JsonResponse<Map<String, Object>> response = new JsonResponse();
        Map<String, Object> data = new HashMap();
        PaymentMethod paymentMethod = new PaymentMethod();

        if (errors.hasErrors()) {

            errors.getAllErrors().forEach(ex -> {
                Source source = new Source();
                source.setPointer(request.getDescription(false));
                source.setParameter(new String());

                Error error = new Error();
                error.setCode("400");
                error.setSource(source);
                error.setDetail(ex.getDefaultMessage());
                error.setTitle("Validation Failed");
                error.setStatus(HttpStatus.BAD_REQUEST.getReasonPhrase());

                response.getErrors().add(error);

            });
            data.put(PERSONAL_REPORT_RESPONSE, paymentMethodRequest);
            response.setData(data);
            return new ResponseEntity(response, HttpStatus.BAD_REQUEST);
        }

        try {
            BeanUtils.copyProperties(paymentMethodRequest, paymentMethod);

            paymentMethod.setStatus(Status.ACTIVE);
            if (paymentMethodRequest.getStatus() != null)
                paymentMethod.setStatus(paymentMethodRequest.getStatus());
            paymentMethod.setCreateAt(LocalDateTime.now());
            paymentMethod.setCreateBy("system");
            paymentMethod.setCreateIp(httpServletRequest.getRemoteAddr());
            PaymentMethod paymentMethodSave = paymentMethodService.save(paymentMethod);
            data.put(PERSONAL_REPORT_RESPONSE, paymentMethodSave);
            response.setData(data);
        } catch (Exception ex) {
            Error error = new Error();
            error.setCode("500");
            error.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.toString());
            error.setTitle(ex.getMessage());
            error.setDetail(ex.getMessage());
            return new ResponseEntity(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity(response, HttpStatus.CREATED);
    }

    @PutMapping(value = "update")
    public ResponseEntity<?> paymentMethodUpdateIdPut(WebRequest request, HttpServletRequest httpServletRequest,
                                             @Valid @RequestBody PaymentMethodRequest paymentMethodRequest, Errors errors, @RequestParam("id") String id) {

        JsonResponse<Map<String, Object>> response = new JsonResponse();

        Map<String, Object> data = new HashMap();
        Map<String, Object> params = new HashMap();
        params.put("id", id);

        Map<String, Object> dictionaries = new HashMap();
        dictionaries.put("params", params);
        response.setDictionaries(dictionaries);

        if (errors.hasErrors()) {

            errors.getAllErrors().forEach(ex -> {
                Source source = new Source();
                source.setPointer(request.getDescription(false));
                source.setParameter(new String());

                Error error = new Error();
                error.setCode("400");
                error.setSource(source);
                error.setDetail(ex.getDefaultMessage());
                error.setTitle("Validation Failed");
                error.setStatus(HttpStatus.BAD_REQUEST.getReasonPhrase());

                response.getErrors().add(error);

            });
            data.put(PERSONAL_REPORT_RESPONSE, paymentMethodRequest);
            response.setData(data);
            return new ResponseEntity(response, HttpStatus.BAD_REQUEST);
        }

        try {
            PaymentMethod paymentMethodValid = paymentMethodService.findById(id);

            BeanUtils.copyProperties(paymentMethodRequest, paymentMethodValid);

            paymentMethodValid.setStatus(Status.ACTIVE);
            if (paymentMethodRequest.getStatus() != null)
                paymentMethodValid.setStatus(paymentMethodRequest.getStatus());
            paymentMethodValid.setModifiedAt(LocalDateTime.now());
            paymentMethodValid.setModifiedBy("system");
            paymentMethodValid.setModifiedIp(httpServletRequest.getRemoteAddr());

            PaymentMethod paymentMethodSave = paymentMethodService.save(paymentMethodValid);
            data.put(PERSONAL_REPORT_RESPONSE, paymentMethodSave);
            response.setData(data);

        } catch (Exception ex) {
            Error error = new Error();
            error.setCode("500");
            error.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.toString());
            error.setTitle(ex.getMessage());
            error.setDetail(ex.getMessage());

            return new ResponseEntity(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity(response, HttpStatus.OK);
    }

    @DeleteMapping(value = "delete")
    public ResponseEntity<Map<String, Object>> paymentMethodDeleteIdDelete(WebRequest request,
                                                                  HttpServletRequest httpServletRequest,
                                                                  @RequestParam("id") String id) {

        JsonResponse<Map<String, Object>> response = new JsonResponse();

        Map<String, Object> data = new HashMap();
        Map<String, Object> params = new HashMap();
        params.put("id", id);

        Map<String, Object> dictionaries = new HashMap();
        dictionaries.put("params", params);
        response.setDictionaries(dictionaries);
        try {
            PaymentMethod paymentMethod = paymentMethodService.findById(id);
            if (paymentMethod == null) {
                Error error = new Error();
                error.setCode("202");
                error.setStatus(HttpStatus.ACCEPTED.toString());
                error.setTitle("PaymentMethod Not Found");
                error.setDetail("PaymentMethod Not found");

                return new ResponseEntity(response, HttpStatus.ACCEPTED);
            }

            paymentMethod.setModifiedIp(httpServletRequest.getRemoteAddr());
            paymentMethod.setModifiedAt(LocalDateTime.now());
            paymentMethod.setModifiedBy("system");
            paymentMethod.setStatus(Status.DELETE);

            paymentMethodService.delete(paymentMethod);

            data.put(PERSONAL_REPORT_DELETE_RESPONSE, paymentMethod);

            response.setData(data);

        } catch (Exception ex) {
            Error error = new Error();
            error.setCode("500");
            error.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.toString());
            error.setTitle(ex.getMessage());
            error.setDetail(ex.getMessage());

            return new ResponseEntity(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return new ResponseEntity(response, HttpStatus.OK);

    }

    @GetMapping(value = "detail")
    public ResponseEntity<JsonResponse<PaymentMethod>> paymentMethodDetailIdGet(WebRequest request, @RequestParam("id") String id) {

        JsonResponse<Map<String, Object>> response = new JsonResponse();

        Map<String, Object> data = new HashMap();
        Map<String, Object> params = new HashMap();
        params.put("id", id);

        Map<String, Object> dictionaries = new HashMap();
        dictionaries.put("params", params);
        response.setDictionaries(dictionaries);

        try {
            PaymentMethod paymentMethod = paymentMethodService.findById(id);
            if (paymentMethod == null) {
                Error error = new Error();
                error.setCode("202");
                error.setStatus(HttpStatus.ACCEPTED.toString());
                error.setTitle("PaymentMethod Not Found");
                error.setDetail("PaymentMethod Not found");

                return new ResponseEntity(response, HttpStatus.ACCEPTED);
            }
            data.put(PERSONAL_REPORT_RESPONSE, paymentMethod);
            response.setData(data);

        } catch (Exception ex) {
            Error error = new Error();
            error.setCode("500");
            error.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.toString());
            error.setTitle(ex.getMessage());
            error.setDetail(ex.getMessage());

            return new ResponseEntity(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity(response, HttpStatus.OK);
    }

}
