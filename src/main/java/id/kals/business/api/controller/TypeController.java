package id.kals.business.api.controller;

import id.kals.business.api.request.TypeRequest;
import id.kals.business.model.entity.Type;
import id.kals.business.model.enums.Status;
import id.kals.business.model.model.Error;
import id.kals.business.model.model.JsonResponse;
import id.kals.business.model.model.Source;
import id.kals.business.model.service.TypeService;
import id.kals.business.model.service.PersonalService;
import id.kals.business.model.utils.BaseSpecification;
import id.kals.business.model.utils.PageUtils;
import id.kals.business.model.utils.SearchCriteria;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.WebRequest;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Author arif <m.arif.kurniawan@myindo.co.id>
 * @Since 9/14/20
 */
@Slf4j
@RestController
@RequestMapping("/type")
public class TypeController {

    @Autowired
    private TypeService typeService;

    @Autowired
    private PersonalService personalService;

    private String PERSONAL_REPORT_RESPONSE = "typeDetail";
    private String PERSONAL_REPORT_DELETE_RESPONSE = "typeDelete";

    @GetMapping(value = "list")
    public ResponseEntity<?> typeListGet(@RequestParam("orderBy") String orderBy,
                                         @RequestParam("sortBy") String sortBy,
                                         @RequestParam("page") int page,
                                         @RequestParam("size") int size,
                                         @RequestParam(required = false, defaultValue = "") String name) {


        BaseSpecification<Type> spec1 =
                new BaseSpecification<Type>(new SearchCriteria("name", ":", name));

        BaseSpecification<Type> spec2 =
                new BaseSpecification<Type>(new SearchCriteria("status", ":", Status.DELETE));


        Specification<Type> specGroup = Specification.where(spec1).and(Specification.not(spec2));
        JsonResponse<List<Type>> response = new JsonResponse<>();
        Map<String, Object> dictionaries = new HashMap<>();
        Map<String, Object> data = new HashMap<>();

        PageUtils pageUtils = new PageUtils();
        try {

            Page<Type> list = typeService.findAllSpecification(specGroup, pageUtils.pagingAndSorting(orderBy, sortBy, page - 1, size));
            if (list == null) {
                Error error = new Error();
                error.setCode("202");
                error.setStatus(HttpStatus.ACCEPTED.toString());
                error.setTitle("Data Not Found");
                error.setDetail("List Not Found Or Empty");
            }

            List<Type> typeList = list.getContent();

            dictionaries.put("totalPages", list.getTotalPages());
            dictionaries.put("totalElements", list.getTotalElements());
            dictionaries.put("numberOfElements", list.getNumberOfElements());
            dictionaries.put("pageSize", list.getSize());
            dictionaries.put("number", list.getNumber());
            dictionaries.put("orderBy", orderBy);
            dictionaries.put("sortBy", sortBy);
            data.put("pagging", dictionaries);

            response.setData(typeList);
            response.setDictionaries(data);

        } catch (Exception ex) {

            Error error = new Error();
            error.setCode("500");
            error.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.toString());
            error.setTitle("Something Error");
            error.setDetail("Something Error, please contact developer");

            return new ResponseEntity(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return new ResponseEntity<>(response, HttpStatus.OK);

    }


    @PostMapping(value = "add")
    public ResponseEntity<?> typeAddPost(
            WebRequest request, HttpServletRequest httpServletRequest,
            @Valid @RequestBody TypeRequest typeRequest, Errors errors) {

        JsonResponse<Map<String, Object>> response = new JsonResponse();
        Map<String, Object> data = new HashMap();
        Type type = new Type();

        if (errors.hasErrors()) {

            errors.getAllErrors().forEach(ex -> {
                Source source = new Source();
                source.setPointer(request.getDescription(false));
                source.setParameter(new String());

                Error error = new Error();
                error.setCode("400");
                error.setSource(source);
                error.setDetail(ex.getDefaultMessage());
                error.setTitle("Validation Failed");
                error.setStatus(HttpStatus.BAD_REQUEST.getReasonPhrase());

                response.getErrors().add(error);

            });
            data.put(PERSONAL_REPORT_RESPONSE, typeRequest);
            response.setData(data);
            return new ResponseEntity(response, HttpStatus.BAD_REQUEST);
        }

        try {
            BeanUtils.copyProperties(typeRequest, type);

            type.setStatus(Status.ACTIVE);
            if (typeRequest.getStatus() != null)
                type.setStatus(typeRequest.getStatus());
            type.setCreateAt(LocalDateTime.now());
            type.setCreateBy("system");
            type.setCreateIp(httpServletRequest.getRemoteAddr());
            Type typeSave = typeService.save(type);
            data.put(PERSONAL_REPORT_RESPONSE, typeSave);
            response.setData(data);
        } catch (Exception ex) {
            Error error = new Error();
            error.setCode("500");
            error.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.toString());
            error.setTitle(ex.getMessage());
            error.setDetail(ex.getMessage());
            return new ResponseEntity(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity(response, HttpStatus.CREATED);
    }

    @PutMapping(value = "update")
    public ResponseEntity<?> typeUpdateIdPut(WebRequest request, HttpServletRequest httpServletRequest,
                                             @Valid @RequestBody TypeRequest typeRequest, Errors errors, @RequestParam("id") String id) {

        JsonResponse<Map<String, Object>> response = new JsonResponse();

        Map<String, Object> data = new HashMap();
        Map<String, Object> params = new HashMap();
        params.put("id", id);

        Map<String, Object> dictionaries = new HashMap();
        dictionaries.put("params", params);
        response.setDictionaries(dictionaries);

        if (errors.hasErrors()) {

            errors.getAllErrors().forEach(ex -> {
                Source source = new Source();
                source.setPointer(request.getDescription(false));
                source.setParameter(new String());

                Error error = new Error();
                error.setCode("400");
                error.setSource(source);
                error.setDetail(ex.getDefaultMessage());
                error.setTitle("Validation Failed");
                error.setStatus(HttpStatus.BAD_REQUEST.getReasonPhrase());

                response.getErrors().add(error);

            });
            data.put(PERSONAL_REPORT_RESPONSE, typeRequest);
            response.setData(data);
            return new ResponseEntity(response, HttpStatus.BAD_REQUEST);
        }

        try {
            Type typeValid = typeService.findById(id);

            BeanUtils.copyProperties(typeRequest, typeValid);

            typeValid.setStatus(Status.ACTIVE);
            if (typeRequest.getStatus() != null)
                typeValid.setStatus(typeRequest.getStatus());
            typeValid.setModifiedAt(LocalDateTime.now());
            typeValid.setModifiedBy("system");
            typeValid.setModifiedIp(httpServletRequest.getRemoteAddr());

            Type typeSave = typeService.save(typeValid);
            data.put(PERSONAL_REPORT_RESPONSE, typeSave);
            response.setData(data);

        } catch (Exception ex) {
            Error error = new Error();
            error.setCode("500");
            error.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.toString());
            error.setTitle(ex.getMessage());
            error.setDetail(ex.getMessage());

            return new ResponseEntity(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity(response, HttpStatus.OK);
    }

    @DeleteMapping(value = "delete")
    public ResponseEntity<Map<String, Object>> typeDeleteIdDelete(WebRequest request,
                                                                  HttpServletRequest httpServletRequest,
                                                                  @RequestParam("id") String id) {

        JsonResponse<Map<String, Object>> response = new JsonResponse();

        Map<String, Object> data = new HashMap();
        Map<String, Object> params = new HashMap();
        params.put("id", id);

        Map<String, Object> dictionaries = new HashMap();
        dictionaries.put("params", params);
        response.setDictionaries(dictionaries);
        try {
            Type type = typeService.findById(id);
            if (type == null) {
                Error error = new Error();
                error.setCode("202");
                error.setStatus(HttpStatus.ACCEPTED.toString());
                error.setTitle("Type Not Found");
                error.setDetail("Type Not found");

                return new ResponseEntity(response, HttpStatus.ACCEPTED);
            }

            type.setModifiedIp(httpServletRequest.getRemoteAddr());
            type.setModifiedAt(LocalDateTime.now());
            type.setModifiedBy("system");
            type.setStatus(Status.DELETE);

            typeService.delete(type);

            data.put(PERSONAL_REPORT_DELETE_RESPONSE, type);

            response.setData(data);

        } catch (Exception ex) {
            Error error = new Error();
            error.setCode("500");
            error.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.toString());
            error.setTitle(ex.getMessage());
            error.setDetail(ex.getMessage());

            return new ResponseEntity(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return new ResponseEntity(response, HttpStatus.OK);

    }

    @GetMapping(value = "detail")
    public ResponseEntity<JsonResponse<Type>> typeDetailIdGet(WebRequest request, @RequestParam("id") String id) {

        JsonResponse<Map<String, Object>> response = new JsonResponse();

        Map<String, Object> data = new HashMap();
        Map<String, Object> params = new HashMap();
        params.put("id", id);

        Map<String, Object> dictionaries = new HashMap();
        dictionaries.put("params", params);
        response.setDictionaries(dictionaries);

        try {
            Type type = typeService.findById(id);
            if (type == null) {
                Error error = new Error();
                error.setCode("202");
                error.setStatus(HttpStatus.ACCEPTED.toString());
                error.setTitle("Type Not Found");
                error.setDetail("Type Not found");

                return new ResponseEntity(response, HttpStatus.ACCEPTED);
            }
            data.put(PERSONAL_REPORT_RESPONSE, type);
            response.setData(data);

        } catch (Exception ex) {
            Error error = new Error();
            error.setCode("500");
            error.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.toString());
            error.setTitle(ex.getMessage());
            error.setDetail(ex.getMessage());

            return new ResponseEntity(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity(response, HttpStatus.OK);
    }

}
