package id.kals.business.api.controller;

import id.kals.business.api.request.PersonalReportRequest;
import id.kals.business.model.entity.Personal;
import id.kals.business.model.entity.PersonalReport;
import id.kals.business.model.enums.Status;
import id.kals.business.model.model.Error;
import id.kals.business.model.model.JsonResponse;
import id.kals.business.model.model.Source;
import id.kals.business.model.service.PersonalReportService;
import id.kals.business.model.service.PersonalService;
import id.kals.business.model.utils.BaseSpecification;
import id.kals.business.model.utils.PageUtils;
import id.kals.business.model.utils.SearchCriteria;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.util.StringUtils;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.WebRequest;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Author arif <m.arif.kurniawan@myindo.co.id>
 * @Since 9/14/20
 */
@Slf4j
@RestController
@RequestMapping("/personalReport")
public class PersonalReportController {

    @Autowired
    private PersonalReportService personalReportService;

    @Autowired
    private PersonalService personalService;

    private String PERSONAL_REPORT_RESPONSE = "personalReportDetail";
    private String PERSONAL_REPORT_DELETE_RESPONSE = "personalReportDelete";

    @GetMapping(value = "list")
    public ResponseEntity<?> personalReportListGet(@RequestParam("orderBy") String orderBy,
                                                   @RequestParam("sortBy") String sortBy,
                                                   @RequestParam("page") int page,
                                                   @RequestParam("size") int size,
                                                   @RequestParam(required = false, defaultValue = "") String personalVisitorId,
                                                   @RequestParam(required = false, defaultValue = "") String personalEmployeeId,
                                                   @RequestParam(required = false, defaultValue = "") String personalRespondentId) {


        BaseSpecification<PersonalReport> spec2 =
                new BaseSpecification<PersonalReport>(new SearchCriteria("personalVisitor.id", ":", personalVisitorId));

        BaseSpecification<PersonalReport> spec3 =
                new BaseSpecification<PersonalReport>(new SearchCriteria("personalEmployee.id", ":", personalEmployeeId));

        BaseSpecification<PersonalReport> spec4 =
                new BaseSpecification<PersonalReport>(new SearchCriteria("personalRespondent.id", ":", personalRespondentId));


        BaseSpecification<PersonalReport> spec11 =
                new BaseSpecification<PersonalReport>(new SearchCriteria("status", ":", Status.DELETE));


        Specification<PersonalReport> specGroup = Specification.where(spec2).and(spec3).and(spec4).and(Specification.not(spec11));
        JsonResponse<List<PersonalReport>> response = new JsonResponse<>();
        Map<String, Object> dictionaries = new HashMap<>();
        Map<String, Object> data = new HashMap<>();

        PageUtils pageUtils = new PageUtils();
        try {

            Page<PersonalReport> list = personalReportService.findAllSpecification(specGroup, pageUtils.pagingAndSorting(orderBy, sortBy, page - 1, size));
            if (list == null) {
                Error error = new Error();
                error.setCode("202");
                error.setStatus(HttpStatus.ACCEPTED.toString());
                error.setTitle("Data Not Found");
                error.setDetail("List Not Found Or Empty");
            }

            List<PersonalReport> personalReportList = list.getContent();

            dictionaries.put("totalPages", list.getTotalPages());
            dictionaries.put("totalElements", list.getTotalElements());
            dictionaries.put("numberOfElements", list.getNumberOfElements());
            dictionaries.put("pageSize", list.getSize());
            dictionaries.put("number", list.getNumber());
            dictionaries.put("orderBy", orderBy);
            dictionaries.put("sortBy", sortBy);
            data.put("pagging", dictionaries);

            response.setData(personalReportList);
            response.setDictionaries(data);

        } catch (Exception ex) {

            Error error = new Error();
            error.setCode("500");
            error.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.toString());
            error.setTitle("Something Error");
            error.setDetail("Something Error, please contact developer");

            return new ResponseEntity(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return new ResponseEntity<>(response, HttpStatus.OK);

    }


    @PostMapping(value = "add")
    public ResponseEntity<?> personalReportAddPost(
            WebRequest request, HttpServletRequest httpServletRequest,
            @Valid @RequestBody PersonalReportRequest personalReportRequest, Errors errors) {

        JsonResponse<Map<String, Object>> response = new JsonResponse();
        Map<String, Object> data = new HashMap();
        PersonalReport personalReport = new PersonalReport();

        if (errors.hasErrors()) {

            errors.getAllErrors().forEach(ex -> {
                Source source = new Source();
                source.setPointer(request.getDescription(false));
                source.setParameter(new String());

                Error error = new Error();
                error.setCode("400");
                error.setSource(source);
                error.setDetail(ex.getDefaultMessage());
                error.setTitle("Validation Failed");
                error.setStatus(HttpStatus.BAD_REQUEST.getReasonPhrase());

                response.getErrors().add(error);

            });
            data.put(PERSONAL_REPORT_RESPONSE, personalReportRequest);
            response.setData(data);
            return new ResponseEntity(response, HttpStatus.BAD_REQUEST);
        }

        Personal personalVisit = personalService.findByIdAndStatus(personalReportRequest.getPersonalVisitorId(), Status.ACTIVE);

        if (personalVisit == null) {

            Error error = new Error();
            error.setCode("202");
            error.setStatus(HttpStatus.ACCEPTED.toString());
            error.setTitle("personalVisit Not Found");
            error.setDetail("personalVisit Not Found");
            response.getErrors().add(error);
            return new ResponseEntity(response, HttpStatus.ACCEPTED);
        }
        Personal personalEmployee = null;
        if (!StringUtils.isEmpty(personalReportRequest.getPersonalEmployeId())) {
            personalEmployee = personalService.findByIdAndStatus(personalReportRequest.getPersonalEmployeId(), Status.ACTIVE);

            if (personalEmployee == null) {

                Error error = new Error();
                error.setCode("202");
                error.setStatus(HttpStatus.ACCEPTED.toString());
                error.setTitle("personalEmployee Not Found");
                error.setDetail("personalEmployee Not Found");
                response.getErrors().add(error);
                return new ResponseEntity(response, HttpStatus.ACCEPTED);
            }
        }
        Personal personalRespondent = null;
        if (!StringUtils.isEmpty(personalReportRequest.getPersonalRespondentId())) {
            personalRespondent = personalService.findByIdAndStatus(personalReportRequest.getPersonalRespondentId(), Status.ACTIVE);

            if (personalRespondent == null) {

                Error error = new Error();
                error.setCode("202");
                error.setStatus(HttpStatus.ACCEPTED.toString());
                error.setTitle("personalRespondent Not Found");
                error.setDetail("personalRespondent Not Found");
                response.getErrors().add(error);
                return new ResponseEntity(response, HttpStatus.ACCEPTED);
            }
        }


        try {
            BeanUtils.copyProperties(personalReportRequest, personalReport);

            personalReport.setPersonalVisitor(personalVisit);
            personalReport.setPersonalEmployee(personalEmployee);
            personalReport.setPersonalRespondent(personalRespondent);
            personalReport.setStartDate(LocalDateTime.now());
            personalReport.setStatus(Status.ACTIVE);
            if (personalReportRequest.getStatus() != null)
                personalReport.setStatus(personalReportRequest.getStatus());
            personalReport.setCreateAt(LocalDateTime.now());
            personalReport.setCreateBy("system");
            personalReport.setCreateIp(httpServletRequest.getRemoteAddr());
            Object personalReportSave = null;
            if (personalReportRequest.getPersonalReportDetailList().isEmpty())
                personalReportSave = personalReportService.save(personalReport);
            else
                personalReportSave = personalReportService.saveWithDetail(personalReport, personalReportRequest.getPersonalReportDetailList());
            data.put(PERSONAL_REPORT_RESPONSE, personalReportSave);
            response.setData(data);
        } catch (Exception ex) {
            Error error = new Error();
            error.setCode("500");
            error.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.toString());
            error.setTitle(ex.getMessage());
            error.setDetail(ex.getMessage());
            return new ResponseEntity(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity(response, HttpStatus.CREATED);
    }

    @PutMapping(value = "update")
    public ResponseEntity<?> personalReportUpdateIdPut(WebRequest request, HttpServletRequest httpServletRequest,
                                                       @Valid @RequestBody PersonalReportRequest personalReportRequest, Errors errors, @RequestParam("id") String id) {

        JsonResponse<Map<String, Object>> response = new JsonResponse();

        Map<String, Object> data = new HashMap();
        Map<String, Object> params = new HashMap();
        params.put("id", id);

        Map<String, Object> dictionaries = new HashMap();
        dictionaries.put("params", params);
        response.setDictionaries(dictionaries);

        if (errors.hasErrors()) {

            errors.getAllErrors().forEach(ex -> {
                Source source = new Source();
                source.setPointer(request.getDescription(false));
                source.setParameter(new String());

                Error error = new Error();
                error.setCode("400");
                error.setSource(source);
                error.setDetail(ex.getDefaultMessage());
                error.setTitle("Validation Failed");
                error.setStatus(HttpStatus.BAD_REQUEST.getReasonPhrase());

                response.getErrors().add(error);

            });
            data.put(PERSONAL_REPORT_RESPONSE, personalReportRequest);
            response.setData(data);
            return new ResponseEntity(response, HttpStatus.BAD_REQUEST);
        }

        try {
            PersonalReport personalReportValid = personalReportService.findById(id);
            if (personalReportValid == null) {
                Error error = new Error();
                error.setCode("202");
                error.setStatus(HttpStatus.ACCEPTED.toString());
                error.setTitle("Personal Report Not Found");
                error.setDetail("Personal Report Not found");

                return new ResponseEntity(response, HttpStatus.ACCEPTED);
            }

            Personal personalVisit = personalService.findByIdAndStatus(personalReportRequest.getPersonalVisitorId(), Status.ACTIVE);

            if (personalVisit == null) {

                Error error = new Error();
                error.setCode("202");
                error.setStatus(HttpStatus.ACCEPTED.toString());
                error.setTitle("personalVisit Not Found");
                error.setDetail("personalVisit Not Found");
                response.getErrors().add(error);
                return new ResponseEntity(response, HttpStatus.ACCEPTED);
            }
            Personal personalEmployee = null;
            if (!StringUtils.isEmpty(personalReportRequest.getPersonalEmployeId())) {
                personalEmployee = personalService.findByIdAndStatus(personalReportRequest.getPersonalEmployeId(), Status.ACTIVE);

                if (personalEmployee == null) {

                    Error error = new Error();
                    error.setCode("202");
                    error.setStatus(HttpStatus.ACCEPTED.toString());
                    error.setTitle("personalEmployee Not Found");
                    error.setDetail("personalEmployee Not Found");
                    response.getErrors().add(error);
                    return new ResponseEntity(response, HttpStatus.ACCEPTED);
                }
            }
            Personal personalRespondent = null;
            if (!StringUtils.isEmpty(personalReportRequest.getPersonalRespondentId())) {
                personalRespondent = personalService.findByIdAndStatus(personalReportRequest.getPersonalRespondentId(), Status.ACTIVE);

                if (personalRespondent == null) {

                    Error error = new Error();
                    error.setCode("202");
                    error.setStatus(HttpStatus.ACCEPTED.toString());
                    error.setTitle("personalRespondent Not Found");
                    error.setDetail("personalRespondent Not Found");
                    response.getErrors().add(error);
                    return new ResponseEntity(response, HttpStatus.ACCEPTED);
                }
            }
            BeanUtils.copyProperties(personalReportRequest, personalReportValid);

            personalReportValid.setPersonalVisitor(personalVisit);
            personalReportValid.setPersonalEmployee(personalEmployee);
            personalReportValid.setPersonalRespondent(personalRespondent);
            personalReportValid.setStartDate(LocalDateTime.now());
            if (personalReportRequest.getEndDate() != null)
                personalReportValid.setEndDate(personalReportRequest.getEndDate());
            personalReportValid.setStatus(Status.ACTIVE);
            if (personalReportRequest.getStatus() != null)
                personalReportValid.setStatus(personalReportRequest.getStatus());
            personalReportValid.setModifiedAt(LocalDateTime.now());
            personalReportValid.setModifiedBy("system");
            personalReportValid.setModifiedIp(httpServletRequest.getRemoteAddr());

            PersonalReport personalReportSave = personalReportService.save(personalReportValid);
            data.put(PERSONAL_REPORT_RESPONSE, personalReportSave);
            response.setData(data);

        } catch (Exception ex) {
            Error error = new Error();
            error.setCode("500");
            error.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.toString());
            error.setTitle(ex.getMessage());
            error.setDetail(ex.getMessage());

            return new ResponseEntity(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity(response, HttpStatus.OK);
    }

    @DeleteMapping(value = "delete")
    public ResponseEntity<Map<String, Object>> personalReportDeleteIdDelete(WebRequest request,
                                                                            HttpServletRequest httpServletRequest,
                                                                            @RequestParam("id") String id) {

        JsonResponse<Map<String, Object>> response = new JsonResponse();

        Map<String, Object> data = new HashMap();
        Map<String, Object> params = new HashMap();
        params.put("id", id);

        Map<String, Object> dictionaries = new HashMap();
        dictionaries.put("params", params);
        response.setDictionaries(dictionaries);
        try {
            PersonalReport personalReport = personalReportService.findById(id);
            if (personalReport == null) {
                Error error = new Error();
                error.setCode("202");
                error.setStatus(HttpStatus.ACCEPTED.toString());
                error.setTitle("Personal Report Not Found");
                error.setDetail("Personal Report Not found");

                return new ResponseEntity(response, HttpStatus.ACCEPTED);
            }

            personalReport.setModifiedIp(httpServletRequest.getRemoteAddr());
            personalReport.setModifiedAt(LocalDateTime.now());
            personalReport.setModifiedBy("system");
            personalReport.setStatus(Status.DELETE);

            personalReportService.delete(personalReport);

            data.put(PERSONAL_REPORT_DELETE_RESPONSE, personalReport);

            response.setData(data);

        } catch (Exception ex) {
            Error error = new Error();
            error.setCode("500");
            error.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.toString());
            error.setTitle(ex.getMessage());
            error.setDetail(ex.getMessage());

            return new ResponseEntity(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return new ResponseEntity(response, HttpStatus.OK);

    }

    @GetMapping(value = "detail")
    public ResponseEntity<JsonResponse<PersonalReport>> personalReportDetailIdGet(WebRequest request, @RequestParam("id") String id) {

        JsonResponse<Map<String, Object>> response = new JsonResponse();

        Map<String, Object> data = new HashMap();
        Map<String, Object> params = new HashMap();
        params.put("id", id);

        Map<String, Object> dictionaries = new HashMap();
        dictionaries.put("params", params);
        response.setDictionaries(dictionaries);

        try {
            PersonalReport personalReport = personalReportService.findById(id);
            if (personalReport == null) {
                Error error = new Error();
                error.setCode("202");
                error.setStatus(HttpStatus.ACCEPTED.toString());
                error.setTitle("Personal Report Not Found");
                error.setDetail("Personal Report Not found");

                return new ResponseEntity(response, HttpStatus.ACCEPTED);
            }
            data.put(PERSONAL_REPORT_RESPONSE, personalReport);
            response.setData(data);

        } catch (Exception ex) {
            Error error = new Error();
            error.setCode("500");
            error.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.toString());
            error.setTitle(ex.getMessage());
            error.setDetail(ex.getMessage());

            return new ResponseEntity(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity(response, HttpStatus.OK);
    }

}
