package id.kals.business.api.controller;

import id.kals.business.api.request.CategoryRequest;
import id.kals.business.model.entity.Category;
import id.kals.business.model.enums.Status;
import id.kals.business.model.model.Error;
import id.kals.business.model.model.JsonResponse;
import id.kals.business.model.model.Source;
import id.kals.business.model.service.PersonalService;
import id.kals.business.model.service.CategoryService;
import id.kals.business.model.utils.BaseSpecification;
import id.kals.business.model.utils.PageUtils;
import id.kals.business.model.utils.SearchCriteria;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.WebRequest;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Author arif <m.arif.kurniawan@myindo.co.id>
 * @Since 9/14/20
 */
@Slf4j
@RestController
@RequestMapping("/category")
public class CategoryController {

    @Autowired
    private CategoryService categoryService;

    @Autowired
    private PersonalService personalService;

    private String PERSONAL_REPORT_RESPONSE = "categoryDetail";
    private String PERSONAL_REPORT_DELETE_RESPONSE = "categoryDelete";

    @GetMapping(value = "list")
    public ResponseEntity<?> categoryListGet(@RequestParam("orderBy") String orderBy,
                                         @RequestParam("sortBy") String sortBy,
                                         @RequestParam("page") int page,
                                         @RequestParam("size") int size,
                                         @RequestParam(required = false, defaultValue = "") String name) {


        BaseSpecification<Category> spec1 =
                new BaseSpecification<Category>(new SearchCriteria("name", ":", name));

        BaseSpecification<Category> spec2 =
                new BaseSpecification<Category>(new SearchCriteria("status", ":", Status.DELETE));


        Specification<Category> specGroup = Specification.where(spec1).and(Specification.not(spec2));
        JsonResponse<List<Category>> response = new JsonResponse<>();
        Map<String, Object> dictionaries = new HashMap<>();
        Map<String, Object> data = new HashMap<>();

        PageUtils pageUtils = new PageUtils();
        try {

            Page<Category> list = categoryService.findAllSpecification(specGroup, pageUtils.pagingAndSorting(orderBy, sortBy, page - 1, size));
            if (list == null) {
                Error error = new Error();
                error.setCode("202");
                error.setStatus(HttpStatus.ACCEPTED.toString());
                error.setTitle("Data Not Found");
                error.setDetail("List Not Found Or Empty");
            }

            List<Category> categoryList = list.getContent();

            dictionaries.put("totalPages", list.getTotalPages());
            dictionaries.put("totalElements", list.getTotalElements());
            dictionaries.put("numberOfElements", list.getNumberOfElements());
            dictionaries.put("pageSize", list.getSize());
            dictionaries.put("number", list.getNumber());
            dictionaries.put("orderBy", orderBy);
            dictionaries.put("sortBy", sortBy);
            data.put("pagging", dictionaries);

            response.setData(categoryList);
            response.setDictionaries(data);

        } catch (Exception ex) {

            Error error = new Error();
            error.setCode("500");
            error.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.toString());
            error.setTitle("Something Error");
            error.setDetail("Something Error, please contact developer");

            return new ResponseEntity(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return new ResponseEntity<>(response, HttpStatus.OK);

    }


    @PostMapping(value = "add")
    public ResponseEntity<?> categoryAddPost(
            WebRequest request, HttpServletRequest httpServletRequest,
            @Valid @RequestBody CategoryRequest categoryRequest, Errors errors) {

        JsonResponse<Map<String, Object>> response = new JsonResponse();
        Map<String, Object> data = new HashMap();
        Category category = new Category();

        if (errors.hasErrors()) {

            errors.getAllErrors().forEach(ex -> {
                Source source = new Source();
                source.setPointer(request.getDescription(false));
                source.setParameter(new String());

                Error error = new Error();
                error.setCode("400");
                error.setSource(source);
                error.setDetail(ex.getDefaultMessage());
                error.setTitle("Validation Failed");
                error.setStatus(HttpStatus.BAD_REQUEST.getReasonPhrase());

                response.getErrors().add(error);

            });
            data.put(PERSONAL_REPORT_RESPONSE, categoryRequest);
            response.setData(data);
            return new ResponseEntity(response, HttpStatus.BAD_REQUEST);
        }

        try {
            BeanUtils.copyProperties(categoryRequest, category);

            category.setStatus(Status.ACTIVE);
            if (categoryRequest.getStatus() != null)
                category.setStatus(categoryRequest.getStatus());
            category.setCreateAt(LocalDateTime.now());
            category.setCreateBy("system");
            category.setCreateIp(httpServletRequest.getRemoteAddr());
            Category categorySave = categoryService.save(category);
            data.put(PERSONAL_REPORT_RESPONSE, categorySave);
            response.setData(data);
        } catch (Exception ex) {
            Error error = new Error();
            error.setCode("500");
            error.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.toString());
            error.setTitle(ex.getMessage());
            error.setDetail(ex.getMessage());
            return new ResponseEntity(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity(response, HttpStatus.CREATED);
    }

    @PutMapping(value = "update")
    public ResponseEntity<?> categoryUpdateIdPut(WebRequest request, HttpServletRequest httpServletRequest,
                                             @Valid @RequestBody CategoryRequest categoryRequest, Errors errors, @RequestParam("id") String id) {

        JsonResponse<Map<String, Object>> response = new JsonResponse();

        Map<String, Object> data = new HashMap();
        Map<String, Object> params = new HashMap();
        params.put("id", id);

        Map<String, Object> dictionaries = new HashMap();
        dictionaries.put("params", params);
        response.setDictionaries(dictionaries);

        if (errors.hasErrors()) {

            errors.getAllErrors().forEach(ex -> {
                Source source = new Source();
                source.setPointer(request.getDescription(false));
                source.setParameter(new String());

                Error error = new Error();
                error.setCode("400");
                error.setSource(source);
                error.setDetail(ex.getDefaultMessage());
                error.setTitle("Validation Failed");
                error.setStatus(HttpStatus.BAD_REQUEST.getReasonPhrase());

                response.getErrors().add(error);

            });
            data.put(PERSONAL_REPORT_RESPONSE, categoryRequest);
            response.setData(data);
            return new ResponseEntity(response, HttpStatus.BAD_REQUEST);
        }

        try {
            Category categoryValid = categoryService.findById(id);

            BeanUtils.copyProperties(categoryRequest, categoryValid);

            categoryValid.setStatus(Status.ACTIVE);
            if (categoryRequest.getStatus() != null)
                categoryValid.setStatus(categoryRequest.getStatus());
            categoryValid.setModifiedAt(LocalDateTime.now());
            categoryValid.setModifiedBy("system");
            categoryValid.setModifiedIp(httpServletRequest.getRemoteAddr());

            Category categorySave = categoryService.save(categoryValid);
            data.put(PERSONAL_REPORT_RESPONSE, categorySave);
            response.setData(data);

        } catch (Exception ex) {
            Error error = new Error();
            error.setCode("500");
            error.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.toString());
            error.setTitle(ex.getMessage());
            error.setDetail(ex.getMessage());

            return new ResponseEntity(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity(response, HttpStatus.OK);
    }

    @DeleteMapping(value = "delete")
    public ResponseEntity<Map<String, Object>> categoryDeleteIdDelete(WebRequest request,
                                                                  HttpServletRequest httpServletRequest,
                                                                  @RequestParam("id") String id) {

        JsonResponse<Map<String, Object>> response = new JsonResponse();

        Map<String, Object> data = new HashMap();
        Map<String, Object> params = new HashMap();
        params.put("id", id);

        Map<String, Object> dictionaries = new HashMap();
        dictionaries.put("params", params);
        response.setDictionaries(dictionaries);
        try {
            Category category = categoryService.findById(id);
            if (category == null) {
                Error error = new Error();
                error.setCode("202");
                error.setStatus(HttpStatus.ACCEPTED.toString());
                error.setTitle("Category Not Found");
                error.setDetail("Category Not found");

                return new ResponseEntity(response, HttpStatus.ACCEPTED);
            }

            category.setModifiedIp(httpServletRequest.getRemoteAddr());
            category.setModifiedAt(LocalDateTime.now());
            category.setModifiedBy("system");
            category.setStatus(Status.DELETE);

            categoryService.delete(category);

            data.put(PERSONAL_REPORT_DELETE_RESPONSE, category);

            response.setData(data);

        } catch (Exception ex) {
            Error error = new Error();
            error.setCode("500");
            error.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.toString());
            error.setTitle(ex.getMessage());
            error.setDetail(ex.getMessage());

            return new ResponseEntity(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return new ResponseEntity(response, HttpStatus.OK);

    }

    @GetMapping(value = "detail")
    public ResponseEntity<JsonResponse<Category>> categoryDetailIdGet(WebRequest request, @RequestParam("id") String id) {

        JsonResponse<Map<String, Object>> response = new JsonResponse();

        Map<String, Object> data = new HashMap();
        Map<String, Object> params = new HashMap();
        params.put("id", id);

        Map<String, Object> dictionaries = new HashMap();
        dictionaries.put("params", params);
        response.setDictionaries(dictionaries);

        try {
            Category category = categoryService.findById(id);
            if (category == null) {
                Error error = new Error();
                error.setCode("202");
                error.setStatus(HttpStatus.ACCEPTED.toString());
                error.setTitle("Category Not Found");
                error.setDetail("Category Not found");

                return new ResponseEntity(response, HttpStatus.ACCEPTED);
            }
            data.put(PERSONAL_REPORT_RESPONSE, category);
            response.setData(data);

        } catch (Exception ex) {
            Error error = new Error();
            error.setCode("500");
            error.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.toString());
            error.setTitle(ex.getMessage());
            error.setDetail(ex.getMessage());

            return new ResponseEntity(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity(response, HttpStatus.OK);
    }

}
