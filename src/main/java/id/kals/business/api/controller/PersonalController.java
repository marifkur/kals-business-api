package id.kals.business.api.controller;

import id.kals.business.model.entity.Personal;
import id.kals.business.model.enums.Status;
import id.kals.business.model.model.Error;
import id.kals.business.model.model.JsonResponse;
import id.kals.business.model.service.PersonalService;
import id.kals.business.model.utils.BaseSpecification;
import id.kals.business.model.utils.PageUtils;
import id.kals.business.model.utils.SearchCriteria;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Author arif <m.arif.kurniawan@myindo.co.id>
 * @Since 9/14/20
 */
@Slf4j
@RestController
@RequestMapping("/personal")
public class PersonalController {


    @Autowired
    private PersonalService personalService;

    private String PERSONAL_RESPONSE = "personalDetail";

    private String PERSONAL_DELETE_RESPONSE = "personalDelete";

    @GetMapping(value = "list")
    public ResponseEntity<?> personalListGet(@RequestParam("orderBy") String orderBy,
                                         @RequestParam("sortBy") String sortBy,
                                         @RequestParam("page") int page,
                                         @RequestParam("size") int size,
                                         @RequestParam(required = false, defaultValue = "") String nickName) {


        BaseSpecification<Personal> spec1 =
                new BaseSpecification<Personal>(new SearchCriteria("nickName", ":", nickName));

        BaseSpecification<Personal> spec2 =
                new BaseSpecification<Personal>(new SearchCriteria("status", ":", Status.DELETE));


        Specification<Personal> specGroup = Specification.where(spec1).and(Specification.not(spec2));
        JsonResponse<List<Personal>> response = new JsonResponse<>();
        Map<String, Object> dictionaries = new HashMap<>();
        Map<String, Object> data = new HashMap<>();

        PageUtils pageUtils = new PageUtils();
        try {

            Page<Personal> list = personalService.findAllSpecification(specGroup, pageUtils.pagingAndSorting(orderBy, sortBy, page - 1, size));
            if (list == null) {
                Error error = new Error();
                error.setCode("202");
                error.setStatus(HttpStatus.ACCEPTED.toString());
                error.setTitle("Data Not Found");
                error.setDetail("List Not Found Or Empty");
            }

            List<Personal> personalList = list.getContent();

            dictionaries.put("totalPages", list.getTotalPages());
            dictionaries.put("totalElements", list.getTotalElements());
            dictionaries.put("numberOfElements", list.getNumberOfElements());
            dictionaries.put("pageSize", list.getSize());
            dictionaries.put("number", list.getNumber());
            dictionaries.put("orderBy", orderBy);
            dictionaries.put("sortBy", sortBy);
            data.put("pagging", dictionaries);

            response.setData(personalList);
            response.setDictionaries(data);

        } catch (Exception ex) {

            Error error = new Error();
            error.setCode("500");
            error.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.toString());
            error.setTitle("Something Error");
            error.setDetail("Something Error, please contact developer");

            return new ResponseEntity(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return new ResponseEntity<>(response, HttpStatus.OK);

    }


}
