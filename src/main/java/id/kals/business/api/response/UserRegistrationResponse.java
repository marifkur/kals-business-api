package id.kals.business.api.response;

import lombok.Getter;
import lombok.Setter;

/**
 * @Author arif <m.arif.kurniawan@myindo.co.id>
 * @Since 9/14/20
 */
@Getter
@Setter
public class UserRegistrationResponse {
}
