package id.kals.business.api.request;

import id.kals.business.model.enums.Status;
import id.kals.business.model.model.PersonalReportDetailModel;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.List;

/**
 * @Author arif <m.arif.kurniawan@myindo.co.id>
 * @Since 9/14/20
 */
@Getter
@Setter
public class PersonalReportRequest {

    private String id;

    @NotEmpty(message = "personalVisitorId can't empty")
    @NotNull(message = "personalVisitorId can't null")
    private String personalVisitorId;

    private String personalEmployeId;

    private String personalRespondentId;

    private LocalDateTime startDate;

    private LocalDateTime endDate;

    @NotEmpty(message = "report can't empty")
    @NotNull(message = "report can't null")
    private String report;

    private String response;

    private String description;

    private Status status;

    private List<PersonalReportDetailModel> personalReportDetailList;
}
