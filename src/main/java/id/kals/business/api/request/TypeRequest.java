package id.kals.business.api.request;

import id.kals.business.model.enums.Status;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

/**
 * @Author arif <m.arif.kurniawan@myindo.co.id>
 * @Since 9/14/20
 */
@Getter
@Setter
public class TypeRequest {

    private String id;

    @NotEmpty(message = "name can't empty")
    @NotNull(message = "name can't null")
    private String name;

    @NotEmpty(message = "value can't empty")
    @NotNull(message = "value can't null")
    private String value;

    private Status status;

}
