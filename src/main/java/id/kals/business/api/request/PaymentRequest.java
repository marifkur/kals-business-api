package id.kals.business.api.request;

import id.kals.business.model.enums.Status;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

/**
 * @Author arif <m.arif.kurniawan@myindo.co.id>
 * @Since 9/14/20
 */
@Getter
@Setter
public class PaymentRequest {

    private String id;

    @NotEmpty(message = "personalReportId can't empty")
    @NotNull(message = "personalReportId can't null")
    private String personalReportId;

    @NotEmpty(message = "personalReportId can't empty")
    @NotNull(message = "personalReportId can't null")
    private String paymentMethodId;

    private LocalDateTime paymentDate;

    private Double total;

    private String responseFinal;

    private String description;

    private Status status;

}
