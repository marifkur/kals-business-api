package id.kals.business.api.request;

import id.kals.business.model.enums.Gender;
import id.kals.business.model.enums.MaritalStatus;
import id.kals.business.model.enums.Religion;
import id.kals.business.model.enums.Status;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.util.Date;

/**
 * @Author arif <m.arif.kurniawan@myindo.co.id>
 * @Since 9/14/20
 */
@Getter
@Setter
public class UserRegistrationRequest {

    @NotEmpty(message = "categoryTypeId can't empty")
    @NotNull(message = "categoryTypeId can't null")
    private String categoryTypeId;

    @NotEmpty(message = "username can't empty")
    @NotNull(message = "username can't null")
    private String username;

    private String password;

    private String retypePassword;

    @NotEmpty(message = "firstName can't empty")
    @NotNull(message = "firstName can't null")
    private String firstName;

    @NotEmpty(message = "lastName can't empty")
    @NotNull(message = "lastName can't null")
    private String lastName;

    @NotEmpty(message = "nickName can't empty")
    @NotNull(message = "nickName can't null")
    private String nickName;

    @NotNull(message = "gender can't null")
    private Gender gender;

    private MaritalStatus maritalStatus;

    @NotNull(message = "religion can't null")
    private Religion religion;

    @NotNull(message = "birthDate can't null")
    private Date birthDate;

    @NotEmpty(message = "placeOfBirth can't empty")
    @NotNull(message = "placeOfBirth can't null")
    private String placeOfBirth;

    @NotEmpty(message = "motherName can't empty")
    @NotNull(message = "motherName can't null")
    private String motherName;

    private Status status;
}
