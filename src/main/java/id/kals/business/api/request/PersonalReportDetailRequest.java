package id.kals.business.api.request;

import id.kals.business.model.enums.Status;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.ColumnDefault;

import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

/**
 * @Author arif <m.arif.kurniawan@myindo.co.id>
 * @Since 9/14/20
 */
@Getter
@Setter
public class PersonalReportDetailRequest {

    private String id;

    @NotEmpty(message = "personalReportId can't empty")
    @NotNull(message = "personalReportId can't null")
    private String personalReportId;

    @NotEmpty(message = "type can't empty")
    @NotNull(message = "type can't null")
    private String type;

    @NotEmpty(message = "name can't empty")
    @NotNull(message = "name can't null")
    private String name;

    @NotNull(message = "cost can't be null")
    private Double cost;

    @Enumerated(EnumType.ORDINAL)
    @Column(columnDefinition = "status",  length = 1)
    @ColumnDefault(value = "")
    private Status status;
}
