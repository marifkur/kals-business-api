package id.kals.business.api.request;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

/**
 * @Author arif <m.arif.kurniawan@myindo.co.id>
 * @Since 9/14/20
 */
@Getter
@Setter
public class UserRoleUpdateRequest {

    private String id;

    @NotEmpty(message = "roleId can't empty")
    @NotNull(message = "roleId can't null")
    private String roleId;

}
