package id.kals.business.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication(
		scanBasePackages = {
				"id.kals.business.api",
				"id.kals.business.model",
		}
)

@ComponentScan(
		basePackages = {

				"id.kals.business.model.service",
				"id.kals.business.model.model",
				"id.kals.business.model.enums",
				"id.kals.business.model.exceptions",
				"id.kals.business.model.utils",

				"id.kals.business.api.controller",
				"id.kals.business.api.config",
				"id.kals.business.api.request",
				"id.kals.business.api.response",

		}
)

@EnableJpaRepositories(
		basePackages = {
				"id.kals.business.model.repository",
		}
)

@EntityScan(
		basePackages = {
				"id.kals.business.model",
		}
)

public class KalsBusinessApiApplication  {

	public static void main(String[] args) {
		SpringApplication.run(KalsBusinessApiApplication.class, args);
	}

}
